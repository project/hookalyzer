<?php

/**
 * @file
 * Contains \Drupal\hookalyzer\Diff\DiffCollection.
 */

namespace Drupal\hookalyzer\Diff;

/**
 * TODO Add class description.
 */
class DiffCollection implements \IteratorAggregate {

  const ARRAY_COLLECTION = 1;

  protected $name;

  protected $type;

  protected array $diffs = [];

  public function __construct($name, $type = self::ARRAY_COLLECTION) {
    $this->name = $name;
    $this->type = $type;
  }

  public function addDiff($id, DiffInterface $diff) {
    $this->diffs[$id] = $diff;
  }

  public function getIterator(): \ArrayIterator {
    return new \ArrayIterator($this->diffs);
  }

  public function getName() {
    return $this->name;
  }

}
