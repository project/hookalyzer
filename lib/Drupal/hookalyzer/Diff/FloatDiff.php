<?php

/**
 * @file
 * Contains \Drupal\hookalyzer\Diff\FloatDiff.
 */

namespace Drupal\hookalyzer\Diff;

/**
 * Represents a diff between two floats.
 */
class FloatDiff extends IntegerDiff {

}
