<?php

namespace Drupal\hookalyzer\EventSubscriber;

use Drupal\Core\Render\RendererInterface;
use Drupal\hookalyzer\ModuleHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Dumps data from hook inspections onto the page for viewing.
 */
class HookDataDumper implements EventSubscriberInterface {

  /**
   * Module handler.
   *
   * @var \Drupal\hookalyzer\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Constructor for the HookDataDumper service.
   *
   * @param \Drupal\hookalyzer\ModuleHandler $handler
   *   Module handler.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer.
   */
  public function __construct(ModuleHandler $handler, RendererInterface $renderer) {
    $this->moduleHandler = $handler;
    $this->renderer = $renderer;
  }

  /**
   * Inject the hook data onto the page.
   */
  public function onResponse(FilterResponseEvent $event) {
    if (!$this->moduleHandler instanceof ModuleHandler) {
      return;
    }

    if ($event->getRequestType() != HttpKernelInterface::MASTER_REQUEST) {
      return;
    }

    $logs = $this->moduleHandler->getAlterLog();

    $outputs = [];
    foreach ($logs as $cid => $collections) {
      $table = [
        '#theme' => 'hookalyzer_html_table',
        '#cid' => $cid,
        '#collections' => $collections,
      ];

      $outputs[$cid] = $this->renderer->renderPlain($table);
    }
    $content = $event->getResponse()->getContent();
    $pos = strripos($content, '</body>');
    $event->getResponse()
      ->setContent(substr_replace($content, implode('', $outputs) . substr($content, $pos), $pos));
  }

  /**
   * Registers the methods in this class that should be listeners.
   *
   * @return array
   *   An array of event listener definitions.
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::RESPONSE][] = ['onResponse'];

    return $events;
  }

}
